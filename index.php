<!doctype html>
<html <?php language_attributes(); ?>>
	<head>
		<title><?php wp_title('|' , true , 'right'); ?></title>
		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?> >
		<div class='body'>
			<header id='head'>
				<div class='head_wrap' >
					<div class='header'>
						<h1>
							<a class='site-title' href='<?php echo esc_url( home_url('/')); ?>' >
								<?php echo bloginfo('title'); ?>
							</a>
						</h1>
						<div class='white_search'>
							<form action='' method='post'>
								<input type='text' name='s' placeholder='type & enter'/>
							</form>
						</div>
						<!--<h2 class='description'><?php bloginfo('description'); ?></h2>-->
					</div>
				</div>
				<div class='slideshow'>
					<div class='slide-content'>
							<h2><a href='#'>About this theme</a></h2>
							<p class='post-slide'>
								The whitetheme is my new theme on wordpress. im using dark and orange color with
								the background of the themes. and use white for text color and orange for link color.
								<a href='#' class='more'>Continue Reading</a>
							</p>
						</div>
					<div class='slide'>
						<img src='<?php echo get_template_directory_uri(); ?>/img/design.jpg' alt='design.jpg' />
					</div>
				</div>
				<div class='navigasi'>
					<nav class='menu-navigasi'>
						<?php wp_nav_menu(); ?>
					</nav>
				</div>
			</header>
			<div class='main'>
				<div class='content-area'>
					<?php if( is_active_sidebar( 'primary' )) : ?>
					<div class='sidebar'>
						<div class='widget'>
							<?php dynamic_sidebar( 'primary'); ?>
						</div>
					</div>
					<?php endif; ?>
					<?php while( have_posts()) : the_post(); ?>
					<article <?php post_class(); ?> id='post-'<?php the_ID(); ?> >
					<?php if(	has_post_thumbnail() ) : ?>
						<div class='post-thumbnail'>
							<?php the_post_thumbnail(); ?>
						</div>
					<?php endif; ?>
						
					<?php if(is_single()) : ?>
						<h1 class='post-title'><?php the_title(); ?></h1>
					<?php else : ?>
						<h1 class='post-title'><a href='<?php the_permalink(); ?>' ><?php the_title(); ?></a></h1>
					<?php endif; ?>
					
					<?php /** handle the content or the excerpt */ ?>
					<?php if( is_search() ) : ?>
						<div class='post-summary'>
							<?php the_excerpt(); ?>
						</div>
					<?php else : ?>
						<div class='post-content'>
							<?php the_content(); ?>
							<?php wp_link_pages(); ?>
						</div>
					<?php endif; ?>
					<?php /** handle comments of post */ ?>
						<footer class='comment'>
						<?php if(comments_open() && ! is_single() ) : ?>
							<div class='comment-link'>
								<?php comments_popup_link(); ?>
							</div>
						<?php endif; ?>
						<?php if ( is_single() && get_the_author_meta( 'description' ) && is_multi_author() ) : ?>
						<?php get_template_part( 'author-bio' ); ?>
						<?php endif; ?>
						</footer>
					</article>					
					<?php endwhile; ?>
				</div>
			</div>
			<footer id='site-footer'>
				<div class='meta-footer'>
					<a href='<?php esc_url('http://greencorelab.com','white themes'); ?>' >
						<?php printf( __( 'powered by %s'),'WordPress'); ?>
					</a>
				</div>
			</footer>
		</div>
		<?php wp_footer(); ?>
	</body>
</html>
