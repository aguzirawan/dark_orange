<?php
/** function of white theme */
function load_enqueue_scripts(){
	wp_enqueue_style(
		'whitetheme',
		get_template_directory_uri() . '/style.css',
		false
		);
	}
add_action( 'wp_enqueue_scripts' , 'load_enqueue_scripts');

function whitethemes_sidebar(){
	register_sidebar( array(
		'name'			=> __('primary_widget'),
		'id'			=> 'primary',
		'description' 	=> __('Show on righbar'),
		'before_widget'	=> '<aside id="%1$s" class="widget %2$s">',
		'after_widget'	=> '</aside>',
		'before_title'	=> '<h3 class="widget-title">',
		'after_title'	=> '</h3>'
		) );
}
add_action( 'widgets_init' , 'whitethemes_sidebar' );

function my_custom_navigasi($args='') {
	$args['theme_location'] = 'primary';
	$args['show_home'] = 'Home';
	return $args;
	}
add_filter('wp_nav_menu_args' , 'my_custom_navigasi');
